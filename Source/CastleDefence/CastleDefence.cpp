// Copyright Epic Games, Inc. All Rights Reserved.

#include "CastleDefence.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CastleDefence, "CastleDefence" );
