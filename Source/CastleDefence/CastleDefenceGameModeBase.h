// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CastleDefenceGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CASTLEDEFENCE_API ACastleDefenceGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
