
#include "CastleDefenceSettings.h"

UCastleDefenceSettings::UCastleDefenceSettings(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

const FZoneGraphTagFilter UCastleDefenceSettings::GetZoneGraphFilterForFaction(FGameplayTag FactionTag) const
{
	return FactionTagToLaneFilter[FactionTag];
}