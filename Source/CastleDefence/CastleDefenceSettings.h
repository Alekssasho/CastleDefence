#pragma once

#include "Engine/DeveloperSettings.h"
#include "GameplayTagContainer.h"
#include "ZoneGraphTypes.h"

#include "CastleDefenceSettings.generated.h"

UCLASS(config = Game, defaultconfig, DisplayName = "Castle Defence")
class UCastleDefenceSettings : public UDeveloperSettings
{
	GENERATED_BODY()
public:
	UCastleDefenceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	const FZoneGraphTagFilter GetZoneGraphFilterForFaction(FGameplayTag FactionTag) const;

protected:
	UPROPERTY(EditAnywhere, config);
	TMap<FGameplayTag, FZoneGraphTagFilter> FactionTagToLaneFilter;
};