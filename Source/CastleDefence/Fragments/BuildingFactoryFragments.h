// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTypes.h"
#include "MassEntityTemplate.h"
#include "BuildingFactoryFragments.generated.h"

USTRUCT()
struct FBuildingFactoryFragment : public FMassFragment
{
	GENERATED_BODY()

	FMassEntityTemplateID TemplateIdToSpawn;
	uint32_t SpawnCount;
	float SpawnTime;

	UPROPERTY(Transient)
	float Timer;
};
