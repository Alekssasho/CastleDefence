// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTypes.h"
#include "CombatFragments.generated.h"

USTRUCT()
struct FHealthFragment : public FMassFragment
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float MaxHealth;

	UPROPERTY(Transient)
	float CurrentHealth;
};

USTRUCT()
struct FCombatTargetFragment : public FMassFragment
{
    GENERATED_BODY()

    UPROPERTY(Transient)
    FMassEntityHandle Target;
};

USTRUCT()
struct FWeaponFragment: public FMassFragment
{
    GENERATED_BODY()

    // TODO: Move this in some kind shared fragment that defines the archetype as a whole
    UPROPERTY(EditAnywhere)
    float DamagePerHit;

    UPROPERTY(EditAnywhere)
    float ReachRadius;

    UPROPERTY(EditAnywhere)
    float CooldownBetweenAttacks;

    UPROPERTY(Transient)
    float TimeLeftBeforeAttack = 0;
};

USTRUCT()
struct FAwarenessRadiusFragment : public FMassFragment
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    float Radius;
};
