// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTypes.h"
#include "GameplayTagContainer.h"
#include "CommonTags.generated.h"

// This should be a tag for now, but Mass Observers for tags are not called when spawning entities.
USTRUCT()
struct FCastleFragment : public FMassFragment
{
	GENERATED_BODY()
};
