// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTypes.h"
#include "ZoneGraphTypes.h"
#include "NavigationFragments.generated.h"

USTRUCT()
struct FNavigationFragment : public FMassFragment
{
	GENERATED_BODY()

	float Progress;

	int CurrentPointIndex;

	FZoneGraphLaneHandle LaneHandle;
};

USTRUCT()
struct FNavigationParameters : public FMassSharedFragment
{
	GENERATED_BODY()

	/** Filter describing which lanes can be used when spawned. */
	UPROPERTY(EditAnywhere, Category = "Navigation")
	FZoneGraphTagFilter LaneFilter;

	/** Query radius when trying to find nearest lane when spawned. */
	UPROPERTY(EditAnywhere, Category = "Navigation", meta = (UIMin = 0.0, ClampMin = 0.0, ForceUnits = "cm"))
	float QueryRadius = 500.0f;
};