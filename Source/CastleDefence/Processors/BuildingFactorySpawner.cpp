// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CastleDefence/Processors/BuildingFactorySpawner.h"

#include "MassCommonFragments.h"
#include "MassSpawnerSubsystem.h"
#include "MassSpawnLocationProcessor.h"
#include "MassSpawnerTypes.h"
#include "MassExecutionContext.h"

#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/BuildingFactoryFragments.h"
#include "CastleDefence/Processors/UnitSpawnerInitializerProcessor.h"

UBuildingFactorySpawnProcessor::UBuildingFactorySpawnProcessor()
	: BuildingFactoryQuery(*this)
{
	bAutoRegisterWithProcessingPhases = true;
	ExecutionFlags = int32(EProcessorExecutionFlags::All);
}

void UBuildingFactorySpawnProcessor::ConfigureQueries()
{
	BuildingFactoryQuery.AddRequirement<FBuildingFactoryFragment>(EMassFragmentAccess::ReadWrite);
	BuildingFactoryQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadOnly);
	BuildingFactoryQuery.AddRequirement<FFactionFragment>(EMassFragmentAccess::ReadOnly);
	// NB: This is probably not needed as we use AsyncTask and not directly using the Subsystem
	//BuildingFactoryQuery.AddSubsystemRequirement<UMassSpawnerSubsystem>(EMassFragmentAccess::ReadWrite);
}

void UBuildingFactorySpawnProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	BuildingFactoryQuery.ForEachEntityChunk(EntityManager, Context, [this, &EntityManager](FMassExecutionContext& Context) {
		const int32 NumEntities = Context.GetNumEntities();
		const TConstArrayView<FTransformFragment> TransformList = Context.GetFragmentView<FTransformFragment>();
		const TConstArrayView<FFactionFragment> FactionList = Context.GetFragmentView<FFactionFragment>();
		const TArrayView<FBuildingFactoryFragment> BuildingFactoryFragment = Context.GetMutableFragmentView<FBuildingFactoryFragment>();

		UMassSpawnerSubsystem* SpawnerSubsystem = UWorld::GetSubsystem<UMassSpawnerSubsystem>(Context.GetWorld());
		ensure(SpawnerSubsystem);

		const float DeltaTime = Context.GetDeltaTimeSeconds();

		for (int32 i = 0; i < NumEntities; ++i)
		{
			BuildingFactoryFragment[i].Timer -= DeltaTime;
			if (BuildingFactoryFragment[i].Timer <= 0.0f)
			{
				BuildingFactoryFragment[i].Timer = BuildingFactoryFragment[i].SpawnTime;

				const FTransform& EntityTransform = TransformList[i].GetTransform();
				const FGameplayTag& BuildingTag = FactionList[i].FactionTag;
				AsyncTask(ENamedThreads::GameThread, [SpawnerSubsystem, TemplateIdToSpawn = BuildingFactoryFragment[i].TemplateIdToSpawn, EntityTransform, BuildingTag]() {
					FInstancedStruct SpawnData;
					SpawnData.InitializeAs<FInitializationData>();
					FInitializationData& AuxData = SpawnData.GetMutable<FInitializationData>();

					AuxData.Transforms.Push(EntityTransform);
					AuxData.FactionTags.Push(BuildingTag);

					TArray<FMassEntityHandle> SpawnedEntities;
					SpawnerSubsystem->SpawnEntities(TemplateIdToSpawn, 1, SpawnData, UInitializationProcessor::StaticClass(), SpawnedEntities);
				});
			}
		}
	});
}
