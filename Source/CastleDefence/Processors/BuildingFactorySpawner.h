// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassProcessor.h"
#include "BuildingFactorySpawner.generated.h"

UCLASS()
class UBuildingFactorySpawnProcessor : public UMassProcessor
{
	GENERATED_BODY()
public:
	UBuildingFactorySpawnProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery BuildingFactoryQuery;
};