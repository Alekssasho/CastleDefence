// Copyright Epic Games, Inc. All Rights Reserved.

#include "CastleSpawnerProcessor.h"
#include "MassCommonFragments.h"
#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/CommonTags.h"
#include "CastleDefence/Subsystems/CastleSubsystem.h"
#include "MassEntityManager.h"
#include "MassExecutionContext.h"
#include "MassSpawnerTypes.h"
#include "Engine/World.h"

UCastleSubsystemInitializer::UCastleSubsystemInitializer()
	: EntityQuery(*this)
{
	ObservedType = FCastleFragment::StaticStruct();
	Operation = EMassObservedOperation::Add;
	ExecutionFlags = (int32)(EProcessorExecutionFlags::All);
}

void UCastleSubsystemInitializer::ConfigureQueries()
{
	EntityQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FFactionFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddSubsystemRequirement<UCastleSubsystem>(EMassFragmentAccess::ReadWrite);
}

void UCastleSubsystemInitializer::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	EntityQuery.ForEachEntityChunk(EntityManager, Context, [this](FMassExecutionContext& Context)
	{
		UCastleSubsystem& CastleSubsystem = Context.GetMutableSubsystemChecked<UCastleSubsystem>();

		const int32 NumEntities = Context.GetNumEntities();
		const TConstArrayView<FFactionFragment> FactionList = Context.GetFragmentView<FFactionFragment>();
		const TConstArrayView<FTransformFragment> TransformList = Context.GetFragmentView<FTransformFragment>();

		for (int32 EntityIndex = 0; EntityIndex < NumEntities; ++EntityIndex)
		{
			CastleSubsystem.RegisterCastle(FCastleData{
				FactionList[EntityIndex].FactionTag,
				TransformList[EntityIndex].GetTransform().GetLocation()
			});
		}
	});
}

