// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "MassProcessor.h"
#include "MassObserverProcessor.h"
#include "CastleSpawnerProcessor.generated.h"

UCLASS()
class UCastleSubsystemInitializer : public UMassObserverProcessor
{
	GENERATED_BODY()

public:
	UCastleSubsystemInitializer();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;
};
