// Copyright Epic Games, Inc. All Rights Reserved.

#include "CombatProcessors.h"
#include "MassCommonFragments.h"
#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/CombatFragments.h"
#include "CastleDefence/Fragments/CommonTags.h"
#include "CastleDefence/Subsystems/CastleSubsystem.h"
#include "MassEntityManager.h"
#include "MassEntityView.h"
#include "MassExecutionContext.h"
//#include "MassNavigationSubsystem.h"
#include "MassSpawnerTypes.h"
#include "MassSignalSubsystem.h"
//#include "MassComponentHitTypes.h"
#include "Engine/World.h"

UAcquireCombatTargetProcessor::UAcquireCombatTargetProcessor()
	: EntityQuery(*this)
{
	bAutoRegisterWithProcessingPhases = true;
	ExecutionFlags = int32(EProcessorExecutionFlags::All);
}

void UAcquireCombatTargetProcessor::ConfigureQueries()
{
	EntityQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FAwarenessRadiusFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FAgentRadiusFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FFactionFragment>(EMassFragmentAccess::ReadOnly);

	EntityQuery.AddRequirement<FCombatTargetFragment>(EMassFragmentAccess::ReadWrite);

	//EntityQuery.AddSubsystemRequirement<UMassNavigationSubsystem>(EMassFragmentAccess::ReadOnly);

	ProcessorRequirements.AddSubsystemRequirement<UMassSignalSubsystem>(EMassFragmentAccess::ReadWrite);
}

void UAcquireCombatTargetProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	TransientEntitiesToSignal.Reset();

	EntityQuery.ForEachEntityChunk(EntityManager, Context, [this, &EntityManager](FMassExecutionContext& Context)
	{
		const int32 NumEntities = Context.GetNumEntities();
		const TConstArrayView<FMassEntityHandle> Entities = Context.GetEntities();
		const TConstArrayView<FTransformFragment> TransformList = Context.GetFragmentView<FTransformFragment>();
		const TConstArrayView<FAwarenessRadiusFragment> AwarenessRadiusList = Context.GetFragmentView<FAwarenessRadiusFragment>();
		const TConstArrayView<FAgentRadiusFragment> AgentRadiusList = Context.GetFragmentView<FAgentRadiusFragment>();
		const TConstArrayView<FFactionFragment> FactionList = Context.GetFragmentView<FFactionFragment>();
		const TArrayView<FCombatTargetFragment> CombatTargetList = Context.GetMutableFragmentView<FCombatTargetFragment>();

		//const UMassNavigationSubsystem* NavigationSubsystem = Context.GetSubsystem<UMassNavigationSubsystem>();

		//const FNavigationObstacleHashGrid2D& ObstacleGrid = NavigationSubsystem->GetObstacleGrid();
		for (int32 i = 0; i < NumEntities; ++i)
		{
			if (CombatTargetList[i].Target.IsValid())
			{
				if (!EntityManager.IsEntityActive(CombatTargetList[i].Target))
				{
					// Our target is dead, so reset it
					CombatTargetList[i].Target.Reset();
					TransientEntitiesToSignal.Add(Entities[i]);
				}
				else
				{
					// We have target and it is still alive
					continue;
				}
			}

			const FVector Location = TransformList[i].GetTransform().GetLocation();
			const float Extent = AgentRadiusList[i].Radius + AwarenessRadiusList[i].Radius;
			const FVector QueryExtent(Extent, Extent, Extent);
			const FBox QueryBox(Location - QueryExtent, Location + QueryExtent);

			//TArray<FNavigationObstacleHashGrid2D::ItemIDType> NearbyEntities;
			//NearbyEntities.Reserve(16);
			//ObstacleGrid.Query(QueryBox, NearbyEntities);

			// Find closest
            /*FMassEntityHandle ClosestEnemy;
            float ClosestDistance = MAX_FLT;
            for (const FNavigationObstacleHashGrid2D::ItemIDType NearbyEntity : NearbyEntities)
            {
                FMassEntityView EntityView(EntityManager, NearbyEntity.Entity);
                const FFactionFragment& TargetFaction = EntityView.GetFragmentData<FFactionFragment>();
                if (TargetFaction.FactionTag == FactionList[i].FactionTag)
                {
                    continue;
                }
                const FTransformFragment& TargetTransform = EntityView.GetFragmentData<FTransformFragment>();
                const FVector TargetLocation = TargetTransform.GetTransform().GetLocation();
                float Distance = (TargetLocation - Location).SquaredLength();
                if (Distance < ClosestDistance)
                {
                    ClosestDistance = Distance;
                    ClosestEnemy = NearbyEntity.Entity;
                }
            }

            if (CombatTargetList[i].Target != ClosestEnemy)
            {
                TransientEntitiesToSignal.AddUnique(Entities[i]);
            }

            CombatTargetList[i].Target = ClosestEnemy;*/
		}
	});

    //if (TransientEntitiesToSignal.Num())
    //{
    //    UMassSignalSubsystem& SignalSubsystem = Context.GetMutableSubsystemChecked<UMassSignalSubsystem>();
    //    SignalSubsystem.SignalEntities(UE::Mass::Signals::HitReceived, TransientEntitiesToSignal);
    //}
}

UWeaponAttackProcessor::UWeaponAttackProcessor()
	: EntityQuery(*this)
{
	bAutoRegisterWithProcessingPhases = true;
	ExecutionFlags = int32(EProcessorExecutionFlags::All);
}

void UWeaponAttackProcessor::ConfigureQueries()
{
    EntityQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadOnly);
    EntityQuery.AddRequirement<FCombatTargetFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FWeaponFragment>(EMassFragmentAccess::ReadWrite);
}

void UWeaponAttackProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	EntityQuery.ForEachEntityChunk(EntityManager, Context, [this, &EntityManager](FMassExecutionContext& Context)
    {
        const float DeltaTime = Context.GetDeltaTimeSeconds();
        const int32 NumEntities = Context.GetNumEntities();
        TConstArrayView<FTransformFragment> TransformList = Context.GetFragmentView<FTransformFragment>();
        TConstArrayView<FCombatTargetFragment> CombatTargetList = Context.GetFragmentView<FCombatTargetFragment>();
        TArrayView<FWeaponFragment> WeaponList = Context.GetMutableFragmentView<FWeaponFragment>();
		for (int32 i = 0; i < NumEntities; ++i)
		{
			if (!CombatTargetList[i].Target.IsValid())
			{
				continue;
			}
			FWeaponFragment& Weapon = WeaponList[i];
			Weapon.TimeLeftBeforeAttack = FMath::Max(0.0f, Weapon.TimeLeftBeforeAttack - DeltaTime);
			if (Weapon.TimeLeftBeforeAttack > 0.0f)
			{
				continue;
			}

			// TODO: Add agent radius on top of location
			FMassEntityView EnemyView(EntityManager, CombatTargetList[i].Target);
			FVector EnemyLocation = EnemyView.GetFragmentData<FTransformFragment>().GetTransform().GetLocation();
			if ((TransformList[i].GetTransform().GetLocation() - EnemyLocation).SquaredLength() < Weapon.ReachRadius * Weapon.ReachRadius)
			{
				FHealthFragment& EnemyHealth = EnemyView.GetFragmentData<FHealthFragment>();
				EnemyHealth.CurrentHealth -= Weapon.DamagePerHit;
				Weapon.TimeLeftBeforeAttack = Weapon.CooldownBetweenAttacks;
			}
		}
	});
}

UDeathProcessor::UDeathProcessor()
	: EntityQuery(*this)
{
	bAutoRegisterWithProcessingPhases = true;
	ExecutionFlags = int32(EProcessorExecutionFlags::All);
}

void UDeathProcessor::ConfigureQueries()
{
	EntityQuery.AddRequirement<FHealthFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FFactionFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddSubsystemRequirement<UCastleSubsystem>(EMassFragmentAccess::ReadWrite);
}

void UDeathProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	TArray<FMassEntityHandle> EntitiesToDestroy;
	EntityQuery.ForEachEntityChunk(EntityManager, Context, [this, &EntitiesToDestroy, &EntityManager](FMassExecutionContext& Context)
	{
		const int32 NumEntities = Context.GetNumEntities();
		TConstArrayView<FMassEntityHandle> EntityList = Context.GetEntities();
		TConstArrayView<FHealthFragment> HealtList = Context.GetFragmentView<FHealthFragment>();
		TConstArrayView<FFactionFragment> FactionList = Context.GetFragmentView<FFactionFragment>();
		UCastleSubsystem* CastleSubsystem = Context.GetMutableSubsystem<UCastleSubsystem>();

		for (int32 i = 0; i < NumEntities; ++i)
		{
			if (HealtList[i].CurrentHealth <= 0.0f)
			{
				EntitiesToDestroy.Push(EntityList[i]);

				FMassEntityView EntityView(EntityManager, EntityList[i]);
				if (EntityView.GetFragmentDataPtr<FCastleFragment>())
				{
					// Castle has died, so we need to end the game
					CastleSubsystem->CastleDestroyed(FactionList[i].FactionTag);
				}
			}
		}
	});

	EntityManager.Defer().DestroyEntities(EntitiesToDestroy);
}