// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "MassProcessor.h"
#include "CombatProcessors.generated.h"

UCLASS()
class UAcquireCombatTargetProcessor : public UMassProcessor
{
	GENERATED_BODY()

public:
	UAcquireCombatTargetProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;

	TArray<FMassEntityHandle> TransientEntitiesToSignal;
};

UCLASS()
class UWeaponAttackProcessor : public UMassProcessor
{
	GENERATED_BODY()

public:
	UWeaponAttackProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;
};

UCLASS()
class UDeathProcessor : public UMassProcessor
{
	GENERATED_BODY()

public:
	UDeathProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;
};