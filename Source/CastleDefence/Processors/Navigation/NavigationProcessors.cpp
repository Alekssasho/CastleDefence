// Copyright Epic Games, Inc. All Rights Reserved.

#include "NavigationProcessors.h"

#include "CastleDefence/Fragments/Navigation/NavigationFragments.h"
#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/CastleDefenceSettings.h"

#include "MassCommonFragments.h"
#include "MassEntityManager.h"
#include "MassEntityView.h"
#include "MassExecutionContext.h"
#include "MassGameplayExternalTraits.h"
#include "MassMovementFragments.h"

#include "ZoneGraphSubsystem.h"
#include "ZoneGraphQuery.h"

#include "Engine/World.h"

UInitializeNavigationProcessor::UInitializeNavigationProcessor()
	: EntityQuery(*this)
{
	ObservedType = FNavigationFragment::StaticStruct();
	Operation = EMassObservedOperation::Add;
	ExecutionFlags = (int32)(EProcessorExecutionFlags::All);
}

void UInitializeNavigationProcessor::ConfigureQueries()
{
	EntityQuery.AddRequirement<FNavigationFragment>(EMassFragmentAccess::ReadWrite);
	EntityQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddRequirement<FFactionFragment>(EMassFragmentAccess::ReadOnly);
	EntityQuery.AddConstSharedRequirement<FNavigationParameters>(EMassFragmentPresence::All);
	EntityQuery.AddSubsystemRequirement<UZoneGraphSubsystem>(EMassFragmentAccess::ReadOnly);
}

void UInitializeNavigationProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	EntityQuery.ForEachEntityChunk(EntityManager, Context, [this, &EntityManager](FMassExecutionContext& Context)
	{
		const UZoneGraphSubsystem& ZoneGraphSubsystem = Context.GetSubsystemChecked<UZoneGraphSubsystem>();
		const int32 NumEntities = Context.GetNumEntities();
		const TArrayView<FNavigationFragment> NavigationList = Context.GetMutableFragmentView<FNavigationFragment>();
		const TConstArrayView<FTransformFragment> TransformList = Context.GetFragmentView<FTransformFragment>();
		const TConstArrayView<FFactionFragment> FactionList = Context.GetFragmentView<FFactionFragment>();
		const FNavigationParameters& NavigationParams = Context.GetConstSharedFragment<FNavigationParameters>();

		const UCastleDefenceSettings* Settings = GetDefault<UCastleDefenceSettings>();

		for (int32 EntityIndex = 0; EntityIndex < NumEntities; ++EntityIndex)
		{
			FNavigationFragment& Navigation = NavigationList[EntityIndex];
			const FTransformFragment& Transform = TransformList[EntityIndex];
			const FFactionFragment& Faction = FactionList[EntityIndex];
			const FVector& AgentLocation = Transform.GetTransform().GetLocation();

			const FVector QuerySize(NavigationParams.QueryRadius);
			const FBox QueryBounds(AgentLocation - QuerySize, AgentLocation + QuerySize);

			FZoneGraphLaneLocation NearestLane;
			float NearestLaneDistSqr = 0;
			if (ZoneGraphSubsystem.FindNearestLane(QueryBounds, Settings->GetZoneGraphFilterForFaction(Faction.FactionTag), NearestLane, NearestLaneDistSqr))
			{
				Navigation.LaneHandle = NearestLane.LaneHandle;
				Navigation.CurrentPointIndex = NearestLane.LaneSegment;
				Navigation.Progress = NearestLane.DistanceAlongLane;
			}
		}
	});
}

UFollowPathProcessor::UFollowPathProcessor()
	: EntityQuery(*this)
{
	bAutoRegisterWithProcessingPhases = true;
	ExecutionFlags = int32(EProcessorExecutionFlags::All);
}

void UFollowPathProcessor::ConfigureQueries()
{
	EntityQuery.AddRequirement<FMassVelocityFragment>(EMassFragmentAccess::ReadWrite);
	EntityQuery.AddRequirement<FNavigationFragment>(EMassFragmentAccess::ReadWrite);

	// TODO: Maybe we can cache everything and not access this during processing
	EntityQuery.AddSubsystemRequirement<UZoneGraphSubsystem>(EMassFragmentAccess::ReadOnly);
}

void UFollowPathProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	EntityQuery.ForEachEntityChunk(EntityManager, Context, [this, &EntityManager](FMassExecutionContext& Context)
	{
		const float DeltaTime = Context.GetDeltaTimeSeconds();
		const int32 NumEntities = Context.GetNumEntities();

		const UZoneGraphSubsystem& ZoneGraphSubsystem = Context.GetSubsystemChecked<UZoneGraphSubsystem>();

		TArrayView<FMassVelocityFragment> VelocityList = Context.GetMutableFragmentView<FMassVelocityFragment>();
		TArrayView<FNavigationFragment> NavigationList = Context.GetMutableFragmentView<FNavigationFragment>();

		for (int32 EntityIndex = 0; EntityIndex < NumEntities; ++EntityIndex)
		{
			FMassVelocityFragment& Velocity = VelocityList[EntityIndex];
			FNavigationFragment& Navigation = NavigationList[EntityIndex];

			if (!Navigation.LaneHandle.IsValid())
			{
				continue;
			}

			const FZoneGraphStorage& Storage = *ZoneGraphSubsystem.GetZoneGraphStorage(Navigation.LaneHandle.DataHandle);

			const FZoneLaneData& Lane = Storage.Lanes[Navigation.LaneHandle.Index];

			int32 CurrentPoint = Navigation.CurrentPointIndex;

			// Check if we need to go to next point
			if (Navigation.Progress >= Storage.LanePointProgressions[CurrentPoint + 1])
			{
				CurrentPoint++;
				if (CurrentPoint >= Lane.GetLastPoint())
				{
					// This one has finished
					// TODO: Maybe we can just remove the component ? 
					Navigation.LaneHandle.Reset();
					continue;
				}
				Navigation.CurrentPointIndex = CurrentPoint;
			}

			const float MoveSpeed = 100.0f;
			const float MovedDistance = MoveSpeed * DeltaTime;

			FVector Forward = (Storage.LanePoints[CurrentPoint + 1] - Storage.LanePoints[CurrentPoint]).GetSafeNormal();
			Velocity.Value = Forward * MoveSpeed;
			Navigation.Progress += MovedDistance;
		}
	});
}
