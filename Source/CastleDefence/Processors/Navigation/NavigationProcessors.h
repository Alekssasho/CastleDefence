// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "MassProcessor.h"
#include "MassObserverProcessor.h"
#include "NavigationProcessors.generated.h"

UCLASS()
class UInitializeNavigationProcessor : public UMassObserverProcessor
{
	GENERATED_BODY()

public:
	UInitializeNavigationProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;
};

UCLASS()
class UFollowPathProcessor : public UMassProcessor
{
	GENERATED_BODY()

public:
	UFollowPathProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;
};
