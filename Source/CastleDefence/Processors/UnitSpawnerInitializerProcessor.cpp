// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnitSpawnerInitializerProcessor.h"
#include "MassCommonFragments.h"
#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/CommonTags.h"
#include "MassEntityManager.h"
#include "MassExecutionContext.h"
#include "MassSpawnerTypes.h"
#include "Engine/World.h"

UInitializationProcessor::UInitializationProcessor()
	: EntityQuery(*this)
{
	bAutoRegisterWithProcessingPhases = false;
}

void UInitializationProcessor::ConfigureQueries()
{
	EntityQuery.AddRequirement<FTransformFragment>(EMassFragmentAccess::ReadWrite);
	EntityQuery.AddRequirement<FFactionFragment>(EMassFragmentAccess::ReadWrite);
}

void UInitializationProcessor::Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context)
{
	if (!ensure(Context.ValidateAuxDataType<FInitializationData>()))
	{
		return;
	}

	FInitializationData& AuxData = Context.GetMutableAuxData().GetMutable<FInitializationData>();

	EntityQuery.ForEachEntityChunk(EntityManager, Context, [&AuxData, this](FMassExecutionContext& Context)
	{
		const TArrayView<FTransformFragment> LocationList = Context.GetMutableFragmentView<FTransformFragment>();
		const TArrayView<FFactionFragment> FactionList = Context.GetMutableFragmentView<FFactionFragment>();
		const int32 NumEntities = Context.GetNumEntities();
		for (int32 i = 0; i < NumEntities; ++i)
		{
			LocationList[i].GetMutableTransform() = AuxData.Transforms[i];
			FactionList[i].FactionTag = AuxData.FactionTags[i];
		}
	});
}

