// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "MassProcessor.h"
#include "GameplayTagContainer.h"
#include "UnitSpawnerInitializerProcessor.generated.h"

USTRUCT()
struct FInitializationData
{
	GENERATED_BODY()

	TArray<FTransform> Transforms;
	TArray<FGameplayTag> FactionTags;
};

UCLASS()
class UInitializationProcessor : public UMassProcessor
{
	GENERATED_BODY()

public:
	UInitializationProcessor();

protected:
	virtual void ConfigureQueries() override;
	virtual void Execute(FMassEntityManager& EntityManager, FMassExecutionContext& Context) override;

	FMassEntityQuery EntityQuery;
};
