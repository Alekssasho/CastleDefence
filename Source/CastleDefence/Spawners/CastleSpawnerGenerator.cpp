// Copyright Epic Games, Inc. All Rights Reserved.

#include "CastleSpawnerGenerator.h"
#include "GameFramework/Actor.h"
#include "EngineUtils.h"
#include "CastleDefence/Processors/UnitSpawnerInitializerProcessor.h"


UCastleSpawnerGenerator::UCastleSpawnerGenerator()
{
}

void UCastleSpawnerGenerator::Generate(UObject& QueryOwner, TConstArrayView<FMassSpawnedEntityType> EntityTypes, const int32 Count, FFinishedGeneratingSpawnDataSignature& FinishedGeneratingSpawnPointsDelegate) const
{
	if (Count <= 0)
	{
		FinishedGeneratingSpawnPointsDelegate.Execute(TArray<FMassEntitySpawnDataGeneratorResult>());
		return;
	}

	TArray<FTransform> Locations;
	TArray<FGameplayTag> FactionTags;
	UWorld* World = QueryOwner.GetWorld();
	ensure(World);

	for (TActorIterator<AActor> It(World, CastleActorClass); It; ++It)
	{
		AActor* Actor = *It;
		Locations.Push(Actor->GetActorTransform());
		FactionTags.Push(FGameplayTag::RequestGameplayTag(Actor->Tags[0]));
	}

	const int32 LocationCount = Locations.Num();
	int32 LocationIndex = 0;

	TArray<FMassEntitySpawnDataGeneratorResult> Results;
	BuildResultsFromEntityTypes(Count, EntityTypes, Results);
	FMassEntitySpawnDataGeneratorResult& Result = Results[0];
	ensure(Results.Num() == 1);
	ensure(Results[0].NumEntities == Locations.Num());

	Result.SpawnDataProcessor = UInitializationProcessor::StaticClass();
	Result.SpawnData.InitializeAs<FInitializationData>();
	FInitializationData& AuxData = Result.SpawnData.GetMutable<FInitializationData>();

	AuxData.Transforms = Locations;
	AuxData.FactionTags = FactionTags;

	FinishedGeneratingSpawnPointsDelegate.Execute(Results);
}
