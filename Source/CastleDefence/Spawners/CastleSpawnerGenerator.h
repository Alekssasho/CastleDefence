// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "MassEntitySpawnDataGeneratorBase.h"
#include "CastleSpawnerGenerator.generated.h"

UCLASS(BlueprintType, meta=(DisplayName="Castle Spawner Genrator"))
class UCastleSpawnerGenerator : public UMassEntitySpawnDataGeneratorBase
{	
	GENERATED_BODY()

public:
	UCastleSpawnerGenerator();
	
	virtual void Generate(UObject& QueryOwner, TConstArrayView<FMassSpawnedEntityType> EntityTypes, int32 Count, FFinishedGeneratingSpawnDataSignature& FinishedGeneratingSpawnPointsDelegate) const override;

protected:
	UPROPERTY(Category = "Actor Class to Search", EditAnywhere)
	TSubclassOf<AActor> CastleActorClass;
};
