// Copyright Epic Games, Inc. All Rights Reserved.

#include "CastleSubsystem.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"

UCastleSubsystem::UCastleSubsystem()
{
}

FVector UCastleSubsystem::FindCastleLocationToGoForFaction(FGameplayTag Faction) const
{
	for (const FCastleData& Data : Castles)
	{
		// find castle which is from opposite faction
		if (Data.Faction.MatchesTag(Faction))
		{
			continue;
		}

		return Data.Location;
	}

	ensure(false);
	return FVector::Zero();
}

void UCastleSubsystem::RegisterCastle(FCastleData Data)
{
	Castles.Push(Data);
}

void UCastleSubsystem::CastleDestroyed(FGameplayTag Faction)
{
	UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
}