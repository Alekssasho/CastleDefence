// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "MassEntityTypes.h"
#include "GameplayTagContainer.h"
#include "Subsystems/WorldSubsystem.h"
#include "CastleSubsystem.generated.h"

struct FCastleData
{
	FGameplayTag Faction;
	FVector Location;
};

UCLASS()
class UCastleSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()

public:
	UCastleSubsystem();

	void RegisterCastle(FCastleData Data);
	FVector FindCastleLocationToGoForFaction(FGameplayTag Faction) const;

	void CastleDestroyed(FGameplayTag Faction);

protected:

	TArray<FCastleData> Castles;
};

template<>
struct TMassExternalSubsystemTraits<UCastleSubsystem> final
{
	enum
	{
		GameThreadOnly = false
	};
};
