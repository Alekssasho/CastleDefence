// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CastleDefence/Traits/BuildingFactoryTrait.h"

#include "MassCommonFragments.h"
#include "MassEntityTemplateRegistry.h"

#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/BuildingFactoryFragments.h"


void UBuildingFactoryTrait::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const
{
	BuildContext.RequireFragment<FTransformFragment>();
	BuildContext.RequireFragment<FFactionFragment>();

	if (!EntityConfigToSpawn)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Trait for Factory building"));
		return;
	}
	const FMassEntityTemplate& TemplateToSpawn = EntityConfigToSpawn->GetOrCreateEntityTemplate(World);

	FBuildingFactoryFragment& BuildingFragment = BuildContext.AddFragment_GetRef<FBuildingFactoryFragment>();
	BuildingFragment.TemplateIdToSpawn = TemplateToSpawn.GetTemplateID();
	BuildingFragment.SpawnCount = 1;
	BuildingFragment.SpawnTime = 2.0f;
	BuildingFragment.Timer = BuildingFragment.SpawnTime;
}
