// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTraitBase.h"
#include "MassEntityConfigAsset.h"
#include "BuildingFactoryTrait.generated.h"

UCLASS(meta = (DisplayName = "Building Factory Trait"))
class UBuildingFactoryTrait : public UMassEntityTraitBase
{
	GENERATED_BODY()
protected:

	virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const override;

public:
	UPROPERTY(EditAnywhere)
	TObjectPtr<UMassEntityConfigAsset> EntityConfigToSpawn;
};
