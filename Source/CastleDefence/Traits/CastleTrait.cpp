// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CastleDefence/Traits/CastleTrait.h"

#include "MassCommonFragments.h"
#include "MassEntityTemplateRegistry.h"

#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/CommonTags.h"


void UCastleTrait::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const
{
	BuildContext.RequireFragment<FTransformFragment>();

	BuildContext.AddFragment<FFactionFragment>();
	BuildContext.AddFragment<FCastleFragment>();
}
