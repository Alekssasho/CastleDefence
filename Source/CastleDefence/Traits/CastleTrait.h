// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTraitBase.h"
#include "CastleTrait.generated.h"

UCLASS(meta = (DisplayName = "Castle Trait"))
class UCastleTrait : public UMassEntityTraitBase
{
	GENERATED_BODY()
protected:

	virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const override;

public:
};
