// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CastleDefence/Traits/CombatTraits.h"

#include "MassCommonFragments.h"
#include "MassEntityTemplateRegistry.h"
//#include "MassNavigationFragments.h"

#include "CastleDefence/Fragments/FactionFragment.h"
#include "CastleDefence/Fragments/CombatFragments.h"


void UCombatAwarenessTrait::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const
{
	BuildContext.RequireFragment<FWeaponFragment>();
	BuildContext.RequireFragment<FFactionFragment>();
	BuildContext.RequireFragment<FHealthFragment>();
    BuildContext.RequireFragment<FAgentRadiusFragment>();

	// Currently we are reusing tech for navigation obstacles as awareness but this could be changed later
    //BuildContext.AddFragment<FMassNavigationObstacleGridCellLocationFragment>();
	BuildContext.AddFragment<FCombatTargetFragment>();
	FAwarenessRadiusFragment& AwarenessFragment = BuildContext.AddFragment_GetRef<FAwarenessRadiusFragment>();
	AwarenessFragment.Radius = AwarenessRadius;
}

void UFactionTrait::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const
{
	FFactionFragment& FactionFragment = BuildContext.AddFragment_GetRef<FFactionFragment>();
	FactionFragment.FactionTag = FactionTag;
}

void UHealthTrait::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const
{
	FHealthFragment& HealthFragment = BuildContext.AddFragment_GetRef<FHealthFragment>();
	HealthFragment.MaxHealth = Health;
	HealthFragment.CurrentHealth = HealthFragment.MaxHealth;
}


