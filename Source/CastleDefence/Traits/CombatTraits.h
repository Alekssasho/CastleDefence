// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTraitBase.h"
#include "GameplayTagContainer.h"
#include "CombatTraits.generated.h"

UCLASS(meta = (DisplayName = "Combat Awareness Trait"))
class UCombatAwarenessTrait : public UMassEntityTraitBase
{
	GENERATED_BODY()
protected:

	virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const override;

public:

	UPROPERTY(EditAnywhere)
	float AwarenessRadius;
};

UCLASS(meta = (DisplayName = "Faction Trait"))
class UFactionTrait : public UMassEntityTraitBase
{
	GENERATED_BODY()
protected:

	virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const override;

public:
	UPROPERTY(EditAnywhere)
	FGameplayTag FactionTag;
};

UCLASS(meta = (DisplayName = "Health Trait"))
class UHealthTrait : public UMassEntityTraitBase
{
	GENERATED_BODY()
protected:

	virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const override;

public:
	UPROPERTY(EditAnywhere)
	float Health;
};

