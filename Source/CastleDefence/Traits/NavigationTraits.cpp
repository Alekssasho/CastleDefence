// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CastleDefence/Traits/NavigationTraits.h"

#include "CastleDefence/Fragments/FactionFragment.h"

#include "MassCommonFragments.h"
#include "MassEntityTemplateRegistry.h"


void UNavigationTrait::BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const
{
	FMassEntityManager& EntityManager = UE::Mass::Utils::GetEntityManagerChecked(World);

	BuildContext.RequireFragment<FTransformFragment>();
	BuildContext.RequireFragment<FAgentRadiusFragment>();
	BuildContext.RequireFragment<FFactionFragment>();

	BuildContext.AddFragment<FNavigationFragment>();

	const FConstSharedStruct NavigationParamsFragment = EntityManager.GetOrCreateConstSharedFragment(SharedParams);
	BuildContext.AddConstSharedFragment(NavigationParamsFragment);
}


