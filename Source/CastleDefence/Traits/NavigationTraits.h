// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "MassEntityTraitBase.h"
#include "CastleDefence/Fragments/Navigation/NavigationFragments.h"
#include "NavigationTraits.generated.h"

UCLASS(meta = (DisplayName = "Navigation Trait"))
class UNavigationTrait : public UMassEntityTraitBase
{
	GENERATED_BODY()
protected:
	virtual void BuildTemplate(FMassEntityTemplateBuildContext& BuildContext, const UWorld& World) const override;

public:

	UPROPERTY(EditAnywhere)
	FNavigationParameters SharedParams;
};

