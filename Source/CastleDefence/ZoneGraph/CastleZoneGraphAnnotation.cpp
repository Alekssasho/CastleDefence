// Copyright Epic Games, Inc. All Rights Reserved.


#include "CastleZoneGraphAnnotation.h"

void UCastleZoneGraphAnnotationComponent::PostSubsystemsInitialized()
{
	Super::PostSubsystemsInitialized();
}

FZoneGraphTagMask UCastleZoneGraphAnnotationComponent::GetAnnotationTags() const
{
	FZoneGraphTagMask AllTags;
	AllTags.Add(CastleTag);

	return AllTags;
}

void UCastleZoneGraphAnnotationComponent::HandleEvents(const FInstancedStructContainer& Events)
{
}

void UCastleZoneGraphAnnotationComponent::TickAnnotation(const float DeltaTime, FZoneGraphAnnotationTagContainer& AnnotationTagContainer)
{
}
