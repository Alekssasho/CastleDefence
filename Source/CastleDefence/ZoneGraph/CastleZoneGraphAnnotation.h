// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ZoneGraphTypes.h"
#include "ZoneGraphAnnotationComponent.h"
#include "CastleZoneGraphAnnotation.generated.h"

UCLASS(ClassGroup = AI, BlueprintType, meta = (BlueprintSpawnableComponent))
class UCastleZoneGraphAnnotationComponent : public UZoneGraphAnnotationComponent
{
	GENERATED_BODY()

protected:
	virtual void PostSubsystemsInitialized() override;
	virtual FZoneGraphTagMask GetAnnotationTags() const override;
	virtual void HandleEvents(const FInstancedStructContainer& Events) override;
	virtual void TickAnnotation(const float DeltaTime, FZoneGraphAnnotationTagContainer& AnnotationTagContainer) override;

	UPROPERTY(EditAnywhere)
	FZoneGraphTag CastleTag;
};
